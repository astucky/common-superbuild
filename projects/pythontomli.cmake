superbuild_add_project_python_pyproject(pythontomli
  PACKAGE
    tomli
  DEPENDS
    pythonflitcore
  LICENSE_FILES
    LICENSE
  SPDX_LICENSE_IDENTIFIER
    MIT
  SPDX_COPYRIGHT_TEXT
    "Copyright (c) 2021 Taneli Hukkinen"
  )
