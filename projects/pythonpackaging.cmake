superbuild_add_project_python_pyproject(pythonpackaging
  PACKAGE
    packaging
  DEPENDS
    pythonflitcore
  LICENSE_FILES
    LICENSE
    LICENSE.BSD
  SPDX_LICENSE_IDENTIFIER
    BSD-2-Clause
  SPDX_COPYRIGHT_TEXT
    "Copyright (c) Donald Stufft and individual contributors"
  )
