superbuild_add_project_python_pyproject(pythonkiwisolver
  PACKAGE
    kiwisolver
  DEPENDS
    pythonsetuptools
    pythonsetuptoolsscm
    pythoncppy
  LICENSE_FILES
    LICENSE
  SPDX_LICENSE_IDENTIFIER
    BSD-3-Clause
  SPDX_COPYRIGHT_TEXT
    "Copyright (c) 2013, Nucleic Development Team"
  )
